# Dutch translation for gallery-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd. 2014
# This file is distributed under the same license as the gallery-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: gallery-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-07 12:10+0000\n"
"PO-Revision-Date: 2022-11-19 18:57+0000\n"
"Last-Translator: Heimen Stoffels <vistausss@fastmail.com>\n"
"Language-Team: Dutch <https://translate.ubports.com/projects/ubports/gallery-"
"app/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-05 07:16+0000\n"

#: lomiri-gallery-app.desktop.in:4 rc/qml/MediaViewer/PopupPhotoViewer.qml:46
msgid "Gallery"
msgstr "Galerij"

#: lomiri-gallery-app.desktop.in:5
msgid "Photo Viewer"
msgstr "Fotoweergave"

#: lomiri-gallery-app.desktop.in:6
msgid "Browse your photographs"
msgstr "Blader door je foto's"

#: lomiri-gallery-app.desktop.in:7
msgid "Photographs;Pictures;Albums"
msgstr "Foto's;Afbeeldingen;Albums"

#: lomiri-gallery-app.desktop.in:9
msgid "lomiri-gallery-app"
msgstr ""

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:43
#: rc/qml/AlbumEditor/AlbumEditor.qml:36
msgid "Edit album"
msgstr "Album bewerken"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:50
#: rc/qml/AlbumViewer/AlbumViewer.qml:378 rc/qml/Components/DeleteDialog.qml:32
#: rc/qml/Components/DeleteDialog.qml:36 rc/qml/EventsOverview.qml:120
#: rc/qml/MediaViewer/MediaViewer.qml:226
#: rc/qml/MediaViewer/MediaViewer.qml:351 rc/qml/PhotosOverview.qml:193
#: rc/qml/Utility/EditingHUD.qml:66
msgid "Delete"
msgstr "Verwijderen"

#: rc/qml/AlbumEditor/AlbumEditor.qml:39 rc/qml/Components/DeleteDialog.qml:45
#: rc/qml/Components/MediaSelector.qml:93 rc/qml/EventsOverview.qml:166
#: rc/qml/MediaViewer/MediaViewer.qml:238
#: rc/qml/MediaViewer/MediaViewer.qml:279 rc/qml/PhotosOverview.qml:240
#: rc/qml/PickerScreen.qml:289
msgid "Cancel"
msgstr "Annuleren"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:113
msgid "Default"
msgstr "Standaard"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:114
msgid "Blue"
msgstr "Blauw"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:115
msgid "Green"
msgstr "Groen"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:116
msgid "Pattern"
msgstr "Patroon"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:117
msgid "Red"
msgstr "Rood"

#: rc/qml/AlbumViewer/AlbumViewer.qml:107 rc/qml/MainScreen.qml:66
msgid "Album"
msgstr "Album"

#: rc/qml/AlbumViewer/AlbumViewer.qml:372
#: rc/qml/MediaViewer/MediaViewer.qml:341
msgid "Add to album"
msgstr "Toevoegen aan album"

#: rc/qml/AlbumsOverview.qml:229 rc/qml/Components/PopupAlbumPicker.qml:91
msgid "Add new album"
msgstr "Album samenstellen"

#: rc/qml/AlbumsOverview.qml:233 rc/qml/Components/PopupAlbumPicker.qml:101
msgid "New Photo Album"
msgstr "Nieuw fotoalbum"

#: rc/qml/AlbumsOverview.qml:234 rc/qml/Components/PopupAlbumPicker.qml:102
msgid "Subtitle"
msgstr "Bijschrift"

#: rc/qml/AlbumsOverview.qml:241 rc/qml/EventsOverview.qml:109
#: rc/qml/PhotosOverview.qml:182
msgid "Camera"
msgstr "Camera"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:51
msgid "Delete album"
msgstr "Album verwijderen"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:60
msgid "Delete album AND contents"
msgstr "Album mét inhoud verwijderen"

#: rc/qml/Components/EventCard.qml:59
msgid "MMM yyyy"
msgstr "MMM yyyy"

#: rc/qml/Components/EventCard.qml:74
msgid "dd"
msgstr "dd"

#: rc/qml/Components/MediaSelector.qml:59
#: rc/qml/Components/MediaSelector.qml:81
msgid "Add to Album"
msgstr "Toevoegen aan album"

#: rc/qml/Components/PopupAlbumPicker.qml:162 rc/qml/Utility/EditingHUD.qml:77
msgid "Add Photo to Album"
msgstr "Foto toevoegen aan album"

#: rc/qml/Components/UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Delen mislukt"

#: rc/qml/Components/UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Foto's en video's kunnen niet tegelijk worden gedeeld"

#: rc/qml/Components/UnableShareDialog.qml:30
msgid "Ok"
msgstr "Oké"

#: rc/qml/EventsOverview.qml:80 rc/qml/PhotosOverview.qml:103
#, qt-format
msgid "Delete %1 photo"
msgid_plural "Delete %1 photos"
msgstr[0] "%1 foto verwijderen"
msgstr[1] "%1 foto's verwijderen"

#: rc/qml/EventsOverview.qml:86 rc/qml/PhotosOverview.qml:109
#, qt-format
msgid "Delete %1 video"
msgid_plural "Delete %1 videos"
msgstr[0] "%1 video verwijderen"
msgstr[1] "%1 video's verwijderen"

#: rc/qml/EventsOverview.qml:92 rc/qml/PhotosOverview.qml:115
#, qt-format
msgid "Delete %1 media file"
msgid_plural "Delete %1 media files"
msgstr[0] "%1 mediabestand verwijderen"
msgstr[1] "%1 mediabestanden verwijderen"

#: rc/qml/EventsOverview.qml:127 rc/qml/MediaViewer/MediaViewer.qml:362
#: rc/qml/PhotosOverview.qml:200 rc/qml/Utility/EditingHUD.qml:71
msgid "Share"
msgstr "Delen"

#: rc/qml/EventsOverview.qml:141 rc/qml/PhotosOverview.qml:215
msgid "Unselect all"
msgstr "Niets selecteren"

#: rc/qml/EventsOverview.qml:141 rc/qml/PhotosOverview.qml:215
msgid "Select all"
msgstr "Alles selecteren"

#: rc/qml/EventsOverview.qml:156 rc/qml/PhotosOverview.qml:230
#: rc/qml/Utility/EditingHUD.qml:76
msgid "Add"
msgstr "Toevoegen"

#: rc/qml/EventsOverview.qml:208 rc/qml/MediaViewer/MediaViewer.qml:182
#: rc/qml/PhotosOverview.qml:259
msgid "Share to"
msgstr "Delen met"

#: rc/qml/LoadingScreen.qml:39
msgid "Loading…"
msgstr "Bezig met laden…"

#: rc/qml/MainScreen.qml:152
msgid "Albums"
msgstr "Albums"

#: rc/qml/MainScreen.qml:166 rc/qml/MainScreen.qml:205
#: rc/qml/PickerScreen.qml:157
msgid "Events"
msgstr "Gebeurtenissen"

#: rc/qml/MainScreen.qml:203 rc/qml/MainScreen.qml:255
#: rc/qml/PickerScreen.qml:195 rc/qml/PickerScreen.qml:243
msgid "Select"
msgstr "Selecteren"

#: rc/qml/MainScreen.qml:214 rc/qml/MainScreen.qml:257
#: rc/qml/PickerScreen.qml:206
msgid "Photos"
msgstr "Foto's"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a photo"
msgstr "Foto verwijderen"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a video"
msgstr "Video verwijderen"

#: rc/qml/MediaViewer/MediaViewer.qml:249
msgid "Remove a photo from album"
msgstr "Foto uit album verwijderen"

#: rc/qml/MediaViewer/MediaViewer.qml:249
msgid "Remove a video from album"
msgstr "Video uit album verwijderen"

#: rc/qml/MediaViewer/MediaViewer.qml:258
msgid "Remove from Album"
msgstr "Uit album verwijderen"

#: rc/qml/MediaViewer/MediaViewer.qml:269
msgid "Remove from Album and Delete"
msgstr "Uit album en definitief verwijderen"

#: rc/qml/MediaViewer/MediaViewer.qml:316
msgid "Edit"
msgstr "Bewerken"

#: rc/qml/MediaViewer/MediaViewer.qml:372
msgid "Info"
msgstr "Informatie"

#: rc/qml/MediaViewer/MediaViewer.qml:395
msgid "Information"
msgstr "Informatie"

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "Media type: "
msgstr "Soort media: "

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "photo"
msgstr "foto"

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "video"
msgstr "video"

#: rc/qml/MediaViewer/MediaViewer.qml:400
msgid "Media name: "
msgstr "Medianaam: "

#: rc/qml/MediaViewer/MediaViewer.qml:401
msgid "Date: "
msgstr "Datum: "

#: rc/qml/MediaViewer/MediaViewer.qml:402
msgid "Time: "
msgstr "Tijd: "

#: rc/qml/MediaViewer/MediaViewer.qml:408
msgid "ok"
msgstr "Oké"

#: rc/qml/MediaViewer/PhotoEditorPage.qml:26
msgid "Edit Photo"
msgstr "Foto bewerken"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:183
msgid "Toggle Selection"
msgstr "Selectie aan/uit"

#: rc/qml/MediaViewer/SingleMediaViewer.qml:241
msgid "An error has occurred attempting to load media"
msgstr "Er is een fout opgetreden tijdens het laden van de media"

#: rc/qml/PhotosOverview.qml:132 rc/qml/PhotosOverview.qml:175
msgid "Grid Size"
msgstr "Roosteromvang"

#: rc/qml/PhotosOverview.qml:133
msgid "Select the grid size in gu units between 8 and 20 (default is 12)"
msgstr "Kies de roosteromvang in gu-eenheden tussen 8 en 20 (standaard: 12)"

#: rc/qml/PhotosOverview.qml:148
msgid "Finished"
msgstr "Klaar"

#: rc/qml/PickerScreen.qml:295
msgid "Pick"
msgstr "Kiezen"

#: rc/qml/Utility/EditingHUD.qml:67
msgid "Trash;Erase"
msgstr "Prullenbak;Wissen"

#: rc/qml/Utility/EditingHUD.qml:72
msgid "Post;Upload;Attach"
msgstr "Plaatsen;Uploaden;Bijvoegen"

#: rc/qml/Utility/EditingHUD.qml:81
msgid "Undo"
msgstr "Ongedaan maken"

#: rc/qml/Utility/EditingHUD.qml:82
msgid "Cancel Action;Backstep"
msgstr "Actie annuleren;Stap terug"

#: rc/qml/Utility/EditingHUD.qml:86
msgid "Redo"
msgstr "Opnieuw"

#: rc/qml/Utility/EditingHUD.qml:87
msgid "Reapply;Make Again"
msgstr "Actie opnieuw uitvoeren;Stap vooruit"

#: rc/qml/Utility/EditingHUD.qml:91
msgid "Auto Enhance"
msgstr "Automatisch verbeteren"

#: rc/qml/Utility/EditingHUD.qml:92
msgid "Adjust the image automatically"
msgstr "Afbeelding automatisch inpassen"

#: rc/qml/Utility/EditingHUD.qml:93
msgid "Automatically Adjust Photo"
msgstr "Foto automatisch inpassen"

#: rc/qml/Utility/EditingHUD.qml:98
msgid "Rotate"
msgstr "Draaien"

#: rc/qml/Utility/EditingHUD.qml:99
msgid "Turn Clockwise"
msgstr "Naar rechts draaien"

#: rc/qml/Utility/EditingHUD.qml:100
msgid "Rotate the image clockwise"
msgstr "Afbeelding naar rechts draaien"

#: rc/qml/Utility/EditingHUD.qml:105
msgid "Crop"
msgstr "Bijsnijden"

#: rc/qml/Utility/EditingHUD.qml:106
msgid "Trim;Cut"
msgstr "Trimmen;Knippen"

#: rc/qml/Utility/EditingHUD.qml:107
msgid "Crop the image"
msgstr "Afbeelding bijsnijden"

#: rc/qml/Utility/EditingHUD.qml:112
msgid "Revert to Original"
msgstr "Origineel herstellen"

#: rc/qml/Utility/EditingHUD.qml:113
msgid "Discard Changes"
msgstr "Bewerkingen terugdraaien"

#: rc/qml/Utility/EditingHUD.qml:114
msgid "Discard all changes"
msgstr "Alle bewerkingen terugdraaien"

#: rc/qml/Utility/EditingHUD.qml:118
msgid "Exposure"
msgstr "Belichting"

#: rc/qml/Utility/EditingHUD.qml:119
msgid "Adjust the exposure"
msgstr "Belichting aanpassen"

#: rc/qml/Utility/EditingHUD.qml:120
msgid "Underexposed;Overexposed"
msgstr "Onderbelicht;Overbelicht"

#: rc/qml/Utility/EditingHUD.qml:121 rc/qml/Utility/EditingHUD.qml:162
msgid "Confirm"
msgstr "Bevestigen"

#: rc/qml/Utility/EditingHUD.qml:125
msgid "Compensation"
msgstr "Compensatie"

#: rc/qml/Utility/EditingHUD.qml:159
msgid "Color Balance"
msgstr "Kleurbalans"

#: rc/qml/Utility/EditingHUD.qml:160
msgid "Adjust color balance"
msgstr "Kleurbalans aanpassen"

#: rc/qml/Utility/EditingHUD.qml:161
msgid "Saturation;Hue"
msgstr "Verzadiging;Kleurtint"

#: rc/qml/Utility/EditingHUD.qml:166
msgid "Brightness"
msgstr "Helderheid"

#: rc/qml/Utility/EditingHUD.qml:177
msgid "Contrast"
msgstr "Contrast"

#: rc/qml/Utility/EditingHUD.qml:188
msgid "Saturation"
msgstr "Verzadiging"

#: rc/qml/Utility/EditingHUD.qml:199
msgid "Hue"
msgstr "Kleurtint"

#~ msgid "Yes"
#~ msgstr "Ja"

#~ msgid "No"
#~ msgstr "Nee"

#~ msgid "Delete 1 photo"
#~ msgstr "1 foto verwijderen"

#~ msgid "Delete 1 video"
#~ msgstr "1 video verwijderen"

#~ msgid "Delete %1 photos and 1 video"
#~ msgstr "%1 foto's en 1 video verwijderen"

#~ msgid "Delete 1 photo and %1 videos"
#~ msgstr "1 foto en %1 video's verwijderen"

#~ msgid "Delete 1 photo and 1 video"
#~ msgstr "1 foto en 1 video verwijderen"

#~ msgid "Delete %1 photos and %2 videos"
#~ msgstr "%1 foto's en %2 video's verwijderen"
